# INSTRUCCIONES

Sigue las instrucciones

## Desarrollo

Realiza las siguientes acciones:

- Renombrar el archivo *docker-compose.dist.yml* a *docker-compose.yml*.
- Configurar los nombres de los contenedores el archivo *docker-compose.yml*
- Renombrar el archivo *Makefile.dist*  a *Makefile*
- Configurar el nombre del contenedor para el backend y el nombre de la red en el archivo *Makefile*

## Crear proyecto symfony

Crear el esqueleto de un proyecto symfony con lo necesario, si no se especifica la versión elige la stable.

`symfony new --no-git --version=lts  my_project`

Crear un proyecto web symfony con las librerías necesarias

`symfony new --no-git --version=lts --webapp  my_project`

Copiar el contenido de my_project a la raiz del proyecto.

`mv my_project/* .`

Copiar los archivos ocultos que hayan quedado en my_project, agregar el contenido del .gitignore al .gitignore de la raiz.

Eliminar la carpeta my_project
